﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Worker
    {
        public async Task<double> CountAndSendAsync(ITransport transport, ICalculateFibonacci message)
        {
            // Send message 
            transport.SendMessage(message);

            // Count Fibonacci
            var fib = new Fibonacci();
            return await fib.CountAsync(message.Value);
        }
    }
}
