﻿namespace Common
{
    public interface ICalculateFibonacci
    {
        string EndpointAddress { get; set; }

        string Action { get; set; }

        double Value { get; set; }
    }

    public class CalculateFibonacci : ICalculateFibonacci
    {
        public string EndpointAddress { get; set; }
        public string Action { get; set; }
        public double Value { get; set; }
    }
}
