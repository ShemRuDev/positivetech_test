﻿using System.Threading.Tasks;

namespace Common
{
    public class Fibonacci
    {
        public async Task<double> CountAsync(double num)
        {
            // For Simplicity reason we not counting for negative values
            if (num <= 0)
                return await Task.FromResult<double>(0);
            else if (num <= 2)
                return await Task.FromResult<double>(1);
            else
                return await CountAsync(num - 1) + await CountAsync(num - 2);
        }
    }
}
