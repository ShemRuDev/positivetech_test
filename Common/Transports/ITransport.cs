﻿namespace Common
{
    public interface ITransport
    {
        void SendMessage(ICalculateFibonacci message);
    }
}
