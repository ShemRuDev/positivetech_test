﻿using Common;
using log4net;
using log4net.Config;
using MassTransit;
using StructureMap;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace FibonacciCoworker
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        static ILog _logger = LogManager.GetLogger("FibonacciCoworker");

        static IBusControl _busControl;

        public static IBus BusControl
        {
            get { return _busControl; }
        }

        public static string TransportEndpointAddress { get; private set; }
        public static string TransportAction { get; private set; }

        public static Container TransportContainer { get; private set; }

        protected void Application_Start()
        {
            XmlConfigurator.Configure();

            TransportContainer = new Container(c =>
            {
                c.For<ITransport>().Use<MassTransitTransport>();
            });

            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            TransportEndpointAddress = ConfigurationManager.AppSettings["TransportEndpoint"];
            TransportAction = ConfigurationManager.AppSettings["TransportAction"];

            _busControl = ConfigureBus();
            _busControl.Start();

            _logger.Info("MassTransit Bus started for commands issuing");
        }

        protected void Application_End()
        {
            _busControl.Stop();
        }

        private IBusControl ConfigureBus()
        {
            return Bus.Factory.CreateUsingRabbitMq(conf =>
            {
                var host = conf.Host(new Uri("rabbitmq://localhost:5672/accounting"), auth =>
                {
                    auth.Username("Fibonacci");
                    auth.Password("fibonacci");
                });
            });
        }
    }
}
