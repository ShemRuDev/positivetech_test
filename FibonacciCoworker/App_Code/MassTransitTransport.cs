﻿using Common;
using MassTransit;
using System;
using System.Threading.Tasks;

namespace FibonacciCoworker
{
    public class MassTransitTransport : ITransport
    {
        public void SendMessage(ICalculateFibonacci message)
        {
            Task<ISendEndpoint> sendEndpointTask = WebApiApplication.BusControl.GetSendEndpoint(
                new Uri($"{message.EndpointAddress}/{message.Action}"));
            ISendEndpoint sendEndpoint = sendEndpointTask.Result;

            Task sendTask = sendEndpoint.Send(message);
        }
    }
}
