﻿using Common;
using log4net;
using System.Threading.Tasks;
using System.Web.Http;

namespace FibonacciCoworker.Controllers
{
    public class FibonacciController : ApiController
    {
        static ILog _logger = LogManager.GetLogger("FibonacciCoworker");

        // api/fibonacci/15
        public double Get(double num)
        {
            _logger.Debug($"Received request with N={num}");

            var worker = new Worker();
            var message = new CalculateFibonacci
            {
                EndpointAddress = WebApiApplication.TransportEndpointAddress,
                Action = WebApiApplication.TransportAction,
                Value = num + 1
            };
            Task<double> res = worker.CountAndSendAsync(WebApiApplication.TransportContainer.GetInstance<ITransport>(), message);

            _logger.Info($"Calculations result for N={message.Value}: {res.Result}");

            return res.Result;
        }
    }
}
