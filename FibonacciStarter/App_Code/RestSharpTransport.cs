﻿using Common;
using log4net;
using RestSharp;

namespace FibonacciStarter
{
    public class RestSharpTransport : ITransport
    {
        static ILog _logger = LogManager.GetLogger("FibonacciStarter.RestSharpTransport");

        public void SendMessage(ICalculateFibonacci message)
        {
            var restClient = new RestClient(message.EndpointAddress);

            var request = new RestRequest(message.Action, Method.GET);
            request.AddParameter("num", message.Value);

            var result = restClient.Execute<double>(request);

            _logger.Info($"Received result from Coworker: {result.Data}");
        }
    }
}
