﻿using Common;
using log4net;
using MassTransit;
using System.Threading.Tasks;

namespace FibonacciStarter
{
    public class RegisterFibonacciConsumer : IConsumer<ICalculateFibonacci>
    {
        static ILog _logger = LogManager.GetLogger("FibonacciStarter.RegisterFibonacciConsumer");

        public Task Consume(ConsumeContext<ICalculateFibonacci> context)
        {
            ICalculateFibonacci calcFibonacci = context.Message;
            _logger.Debug($"Message with value {calcFibonacci.Value} received");

            var worker = new Worker();
            var fibMessage = new CalculateFibonacci
            {
                EndpointAddress = WebApiApplication.CoworkerEndpointAddress,
                Action = WebApiApplication.CoworkerAction,
                Value = calcFibonacci.Value + 1
            };
            Task<double> res = worker.CountAndSendAsync(WebApiApplication.TransportContainer.GetInstance<ITransport>(), fibMessage);

            _logger.Info($"Calculations result: {res.Result}");

            return Task.FromResult(context.Message);
        }
    }
}