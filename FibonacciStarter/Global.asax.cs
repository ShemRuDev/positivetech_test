﻿using Common;
using log4net;
using log4net.Config;
using MassTransit;
using MassTransit.RabbitMqTransport;
using StructureMap;
using System;
using System.Configuration;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace FibonacciStarter
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        static ILog _logger = LogManager.GetLogger("FibonacciStarter");

        private IBusControl _busControl;

        public static string CoworkerEndpointAddress { get; private set; }
        public static string CoworkerAction { get; private set; }

        public static Container TransportContainer { get; private set; }

        protected void Application_Start()
        {
            XmlConfigurator.Configure();

            TransportContainer = new Container(c =>
            {
                c.For<ITransport>().Use<RestSharpTransport>();
            });

            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            CoworkerEndpointAddress = ConfigurationManager.AppSettings["CoworkerHost"];
            CoworkerAction = ConfigurationManager.AppSettings["CoworkerAction"];

            _busControl = Bus.Factory.CreateUsingRabbitMq(rabbit =>
            {
                IRabbitMqHost rabbitMqHost = rabbit.Host(new Uri("rabbitmq://localhost:5672/accounting"), settings =>
                {
                    settings.Username("Fibonacci");
                    settings.Password("fibonacci");
                });

                rabbit.ReceiveEndpoint(rabbitMqHost, "shem.fibo.queues", conf =>
                {
                    conf.Consumer<RegisterFibonacciConsumer>();
                });
            });

            _busControl.Start();

            _logger.Info("MassTransit Bus started for consuming messages");
        }

        protected void Application_End()
        {
            _busControl.Stop();
        }
    }
}
