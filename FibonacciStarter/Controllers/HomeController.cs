﻿using Common;
using log4net;
using System.Collections.Generic;
using System.Configuration;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FibonacciStarter.Controllers
{
    public class HomeController : Controller
    {
        static ILog _logger = LogManager.GetLogger("FibonacciStarter.HomeController");

        private const int ParallelTasksCount_Default = 3;

        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }

        [HttpPost]
        public ActionResult StartCount(FormCollection fc)
        {
            int parallelTasksCount = ConfigurationManager.AppSettings["ParallelTasksCount"] == null
                ? ParallelTasksCount_Default
                : int.Parse(ConfigurationManager.AppSettings["ParallelTasksCount"].ToString());
            
            List<Task> tasks = new List<Task>();
            Dictionary<int, double> results = new Dictionary<int, double>();

            string input = fc["tb_FibStartValue"];
            int startValue;
            if(int.TryParse(input, out startValue))
            {
                var worker = new Worker();

                for (int i = 0; i < parallelTasksCount; i++)
                {
                    var fibMessage = new CalculateFibonacci
                    {
                        EndpointAddress = WebApiApplication.CoworkerEndpointAddress,
                        Action = WebApiApplication.CoworkerAction,
                        Value = startValue
                    };

                    Task task = worker.CountAndSendAsync(WebApiApplication.TransportContainer.GetInstance<ITransport>(), fibMessage)
                        .ContinueWith((t, s) =>
                        {
                            results.Add((int)s, t.Result);
                        }, i, TaskContinuationOptions.ExecuteSynchronously);
                    tasks.Add(task);
                }

                Task.WaitAll(tasks.ToArray());
            }
            else
            {
                _logger.Error($"Start value must be integer! UserInput: {input}");
            }

            return View("Index", results);
        }
    }
}
